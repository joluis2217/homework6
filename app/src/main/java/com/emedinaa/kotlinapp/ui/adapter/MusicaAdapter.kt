package com.emedinaa.kotlinapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.emedinaa.kotlinapp.R
import com.emedinaa.kotlinapp.model.MusicaEntity
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions

class MusicaAdapter (options: FirebaseRecyclerOptions<MusicaEntity>) : FirebaseRecyclerAdapter<MusicaEntity, MusicaAdapter.MusicaViewHolder>(options) {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MusicaViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        return MusicaViewHolder(inflater.inflate(R.layout.row, viewGroup, false))
    }

    override fun onBindViewHolder(holder: MusicaViewHolder, position: Int, model: MusicaEntity) {
        //val item= getRef(position)
        holder.tvNombre.text= model.name
        holder.tvDescripcion.text= model.description
    }

    class MusicaViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val tvNombre= view.findViewById<TextView>(R.id.tvNombre)
        val tvDescripcion= view.findViewById<TextView>(R.id.tvDescripcion)
    }
}