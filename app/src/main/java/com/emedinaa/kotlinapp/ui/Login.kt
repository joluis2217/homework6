package com.emedinaa.kotlinapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.emedinaa.kotlinapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {


    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = Firebase.auth
        Ui()
    }


    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun Validar(): Boolean {
        val Email = EdtEmail.text.trim()
        val Contrasena = EdtPassword.text.trim()

        if (Email.isEmpty()) {
            return false
        }

        if (Contrasena.isEmpty()) {
            return false
        }
        return true
    }

    private fun logIn(){
        val Email = EdtEmail.text.trim().toString()
        val Contrasena = EdtPassword.text.trim().toString()
        auth.signInWithEmailAndPassword(Email,Contrasena)
            .addOnCompleteListener(this){ task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    operationCompleted(user)
                } else {
                    showMessage("Exception $task.exception")
                }
            }
    }

    private fun Ui() {
        ButtonIngresar.setOnClickListener {
            if (Validar()) {
                logIn()
            }
        }
    }

    private fun operationCompleted(user: FirebaseUser?) {
        Log.v("CONSOLE", "user $user")
        showMessage("Operation completed!")
        startActivity(Intent(this, ListaMusica::class.java))
    }


}