package com.emedinaa.kotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.emedinaa.kotlinapp.model.MusicaEntity
import com.emedinaa.kotlinapp.ui.dialog.MusicaDialogFragment
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_edit_musica.*

class EditMusica : AppCompatActivity(),MusicaDialogFragment.DialogListener {
    private lateinit var mDatabase: DatabaseReference
    private var musica: MusicaEntity?=null

    private var Nombre:String?=null
    private var Descripcion:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_musica)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        mDatabase= FirebaseDatabase.getInstance().reference
        ui()
        populate()
    }



    private fun ui(){
        ButtonEditar.setOnClickListener {
            if(validate()){
                EditarMusica()
            }
        }

        ButtonEliminar.setOnClickListener {
            showNoteDialog()
        }
    }

    private fun validate():Boolean{
        Nombre= EdtNombreEdit.text.toString()
        Descripcion= EdtDescripcionEdit.text.toString()

        if(Nombre.isNullOrEmpty()){
            return false
        }

        if(Descripcion.isNullOrEmpty()){
            return false
        }

        return true
    }

    private fun populate(){
        musica?.let {
            EdtNombreEdit.setText(it?.name)
            EdtDescripcionEdit.setText(it?.description)
        }
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

    private fun showNoteDialog(){
        val noteDialogFragment= MusicaDialogFragment()
        val bundle= Bundle()
        bundle.putString("TITLE","¿Deseas eliminar esta Musica?")
        bundle.putInt("TYPE",100)

        noteDialogFragment.arguments= bundle
        noteDialogFragment.show(supportFragmentManager,"dialog")
    }


    override fun onPositiveListener(any: Any?, type: Int) {
        musica?.let {

            EliminarMusica(it)
        }

    }

    override fun onNegativeListener(any: Any?, type: Int) {}

    private fun EliminarMusica(mMusica:MusicaEntity){
        mMusica.id?.let {
            mDatabase.child("Musica").child(it).removeValue(object: DatabaseReference.CompletionListener{
                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {
                    databaseError?.let {
                        showErrorMessage(databaseError.message)
                    }?:run{
                        finish()
                    }
                }
            })
        }
    }

    private fun EditarMusica() {

        val musicaId = musica?.id
        val musica= MusicaEntity(musicaId,Nombre,Descripcion)

        musicaId?.let {
            mDatabase.child("Musica").child(it).updateChildren(musica.toMap(),object: DatabaseReference.CompletionListener{
                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {
                    databaseError?.let {
                        showErrorMessage(databaseError.message)
                    }?:run{
                        finish()
                    }
                }
            })
        }
    }

    private fun verifyExtras(){
        intent?.extras?.let {
            musica= it.getSerializable("Musica") as MusicaEntity
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}