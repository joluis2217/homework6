package com.emedinaa.kotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.emedinaa.kotlinapp.model.MusicaEntity
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_agregar_musica.*

class AgregarMusica : AppCompatActivity() {



    private lateinit var mDatabase: DatabaseReference

    private var Nombre:String?=null
    private var Descripcion:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_musica)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mDatabase= FirebaseDatabase.getInstance().reference
        ui()
    }

    private fun ui(){
        ButtonAgregarMusica.setOnClickListener {
            if(Validar()){
                AddMusica()
            }
        }
    }

    private fun AddMusica(){
        val MusicaId = mDatabase.child("Musica").push().key
        val musica= MusicaEntity(MusicaId,Nombre,Descripcion)
        MusicaId?.let {
            mDatabase.child("Musica").child(it).setValue(musica,object:DatabaseReference.CompletionListener{
                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {
                    databaseError?.let {dbError ->
                        showErrorMessage(dbError.message)
                    }?:run{
                        finish()
                    }
                }
            })
        }
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

    private fun clearForm(){
        EdtNombreMusica.error=null
        EdtDescripcionMusica.error=null
    }


    private fun Validar():Boolean{
        clearForm()
        Nombre= EdtNombreMusica.text.toString().trim()
        Descripcion= EdtDescripcionMusica.text.toString().trim()

        if(Nombre.isNullOrEmpty()){
            EdtNombreMusica.error="Campo Musica inválido"
            return false
        }

        if(Descripcion.isNullOrEmpty()){
            EdtDescripcionMusica.error="Campo Descripcion inválido"
            return false
        }

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}